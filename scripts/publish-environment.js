import axios from "axios";


const {
  TOKEN,
  PROJECT_ID,
  ENVIRONMENT,
  ELB_URL
} = process.env

if(!TOKEN || !ENVIRONMENT || !ELB_URL || !PROJECT_ID){
  console.error(`WARNING : Missing environment variables !
  TOKEN --> ${ TOKEN  ? 'OK' : 'MISSING'}
  PROJECT_ID --> ${ PROJECT_ID ? 'OK' : 'MISSING'}
  ENVIRONMENT --> ${ENVIRONMENT  ? 'OK' : 'MISSING'}
  ELB_URL --> ${ELB_URL ? 'OK' : 'MISSING' }
  `)
  process.exit(1)
}

const base_url =  "https://gitlab.com/api/v4"

const config = {
  headers: {
    "PRIVATE-TOKEN": TOKEN
  }
}

const getEnvList = async (envName) => {
  try{
    const url = `${base_url}/projects/${PROJECT_ID}/environments`
    console.log("get list url :", url)
    const params = {
      name: envName,
    }
    const { data } = await axios.get(url, {...config, params})
    if (data.length){
      console.log(`${data.length} environment named "${ENVIRONMENT}" find`)
      return data[0].id
    }

    return null

  }catch (err){
    const resp =  err.response
    console.error('Error during get environment list', resp.status, resp.statusText, resp.data)
    process.exit(1);
  }

}

const createEnv = async (envName) => {
  try{
    const url = `${base_url}/projects/${PROJECT_ID}/environments`
    console.log("update url is :", url)
    const postData = {
      name: envName,
      external_url: ELB_URL,
      tier: "staging"
    }
    const { data } = await axios.post(url, postData,{...config })
    console.log(`Environment "${ENVIRONMENT}" created`)
    process.exit(0);
  }catch (err){
    const resp =  err.response
    console.error('Error during environment creation', resp.status, resp.statusText, resp.data)
    process.exit(1);
  }

}

const updateEnv = async (envId) => {
  try{
    const url = `${base_url}/projects/${PROJECT_ID}/environments/${envId}`
    console.log("update url is :", url)
    const postData = {
      external_url: ELB_URL,
    }
    const { data } = await axios.put(url, postData,{...config })
    console.log(`Environment "${ENVIRONMENT}" updated`)
    process.exit(0);
  }catch (err){
    const resp =  err.response
    console.error('Error during update environment', resp.status, resp.statusText, resp.data)
    process.exit(1);
  }

}


const exec = async () => {
  const id = await getEnvList(ENVIRONMENT)
  if(id)
    await updateEnv(id)
  else
    await createEnv(ENVIRONMENT)
}

exec()
